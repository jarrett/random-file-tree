# Random File Tree
This tool creates a directory tree with a random quantity of randomly named folders and files.  
All randomness is constrained by the constants at the beginning of the class definition. 

## I want my very own random file tree!
Ok great! Follow these steps:
1. Load or require random_file_tree.rb
    - `require_relative 'random_file_tree'`.
1. Instantiate a new RandomFileTree object.
    - **Note**: The new file tree will be created upon instantiation.
        - If you don't set `rootdir:`, the root folder will be placed whereever your OS puts temporary directories.
    - To set the directory tree depth, use the `depth:` named argument
        - `talltree = RandomFileTree.new(depth: 3)`
    - To set the directory tree root, use the `rootdir:` named argument
        - `hometree = RandomFileTree.new(rootdir: "/home/user/randomtree")`
        - If you don't set 
    - Both arguments can be set simultaneously
        - `tallhometree = RandomFileTree.new(depth: 3, rootdir: "/home/jarrett/randomtree")`
1. Watch your terminal and rejoice when it spits out the path to your newly created random file tree.

## Misc
To aide in finding folders/files created by this tool, all generated file names are have a `.junk` extension.
