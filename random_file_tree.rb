#!/usr/bin/env ruby

require 'pathname'
require 'tempfile'
require 'tmpdir'
require 'securerandom'

# Creates a directory tree of specified depth with a random
# number of files, child directories, and file sizes
# Oh, and the names are random too
class RandomFileTree
  # Here, we set the constraints of our randomness:
  MB = 1048576
  MIN_FILE_SIZE =  1 # 1 MB = 1024 KB
  MAX_FILE_SIZE = 10 # All file sizes are in Megabytes

  MIN_CHILD_DIR_QTY =  1 
  MAX_CHILD_DIR_QTY = 10 

  MIN_FILE_QTY =  0 
  MAX_FILE_QTY = 10

  MIN_NAME_LEN =  3
  MAX_NAME_LEN = 20

  attr_reader :rootdir
  
  # Create a filetree and return pathname object of the tree's root
  # Depth counting starts at 0, so 1 gives us 2 directory levels
  def initialize(depth: 1, rootdir: nil, dry_init: false)
    if dry_init
      @rootdir = Pathname.new(Dir.pwd)
      puts "Dry Init Mode, not building a tree."
    else
      if rootdir
        @rootdir = Pathname.new(rootdir)
        mk_random_tree(depth, rootdir, first_itr = true)
      else
        @rootdir = Pathname.new(mk_random_tree(depth, rootdir))
      end
    end
  end

  def inspect
    @rootdir
  end

  private

  # build a randomized file tree
  def mk_random_tree(depth, rootdir, first_itr = false)
    if rootdir && (not first_itr)
      # Recursively create another tree (called from mk_child_dir)
      # This keeps going until depth is depleted
      rootdir = Dir.mktmpdir(random_name, rootdir)
    elsif (not rootdir)
      # Create a new tree wherever the OS puts temporary directories
      rootdir = Dir.mktmpdir(random_name)
    end
    mk_random_files(rootdir)
    if depth > 0
      mk_child_dir(depth, rootdir)
    end
    return rootdir
  end

  # builds child directories for file tree
  def mk_child_dir(depth, rootdir, max_child_dir_qty: MAX_CHILD_DIR_QTY,
                   min_child_dir_qty: MIN_CHILD_DIR_QTY)
    child_dir_qty = random_number(min_child_dir_qty, max_child_dir_qty)
    child_dir_qty.times do |i|
      # Each child directory also gets a random name
      dir = Dir.mktmpdir(random_name, rootdir)
      mk_random_files(dir)
      if i == 0 
        # Recurse to create another tree on the first iteration of child_dir_qty
        mk_random_tree(depth - 1, rootdir)
      end
    end
  end
  
  # creates random number of files in specified dir
  def mk_random_files(filedir, max_file_qty: MAX_FILE_QTY, min_file_qty: MIN_FILE_QTY)
    filedir = Pathname.new(filedir)
    file_qty = random_number(min_file_qty, max_file_qty)
    file_qty.times do |i|
      # suffix filenames with .junk for easier greppability
      filename = random_name + ".junk"
      mk_random_file(filedir + filename)
    end
    return file_qty
  end

  # creates a file full of random data
  def mk_random_file(filepath, max_file_size: MAX_FILE_SIZE, min_file_size: MIN_FILE_SIZE)
    File.open(filepath, 'wb') do |f|
      f.write(SecureRandom.random_bytes(random_filesize))
    end
    return filepath
  end

  # generates random names
  def random_name(max_name_len: MAX_NAME_LEN, min_name_len: MIN_NAME_LEN)
    name_len = random_number(min_name_len, max_name_len)
    # SecureRandom.urlsafe_base64 creates strings that are 4/3 of the given number
    # Reducing the given number by 25% means it'll give us the exact size we request
    name_len = (name_len * 0.75).to_i
    return SecureRandom.urlsafe_base64(name_len)
  end
  
  # Returns a byte size between min and max
  def random_filesize(max_file_size: MAX_FILE_SIZE, min_file_size: MIN_FILE_SIZE)
    return random_number(min_file_size, max_file_size) * MB
  end

  # Returns a random number within the given range, inclusive
  def random_number(lowest, highest)
    return lowest + SecureRandom.random_number(highest - lowest)
  end
end
